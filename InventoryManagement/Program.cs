﻿namespace InventoryManagement
{
    class Program
    {
        private static Inventory _inventory;
        private static CommandStore _store;
        static void Main()
        {
            // Load
            _store = CommandStoreHelper.Load();
            _inventory = InventoryHelper.LoadFromStore(_store);

            //var phone = new InventoryItem("Sony Ericson","Mobile Phone");
            //_inventory.AddItem(phone);
            
            // Save
            InventoryHelper.Save(_inventory);
            CommandStoreHelper.Save(_store);
        }
    }
}
