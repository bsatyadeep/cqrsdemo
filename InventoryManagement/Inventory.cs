﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace InventoryManagement
{
    [Serializable]
    public class Inventory
    {
        [XmlIgnore]
        private CommandStore _store;
        public uint InventoryItemSeed
        {
            get => InventoryItem.Seed;
            set => InventoryItem.Seed = value;
        }
        [XmlIgnore]
        private Dictionary<uint,InventoryItem> _itemsByKey = new Dictionary<uint, InventoryItem>();

        public Inventory()
        {
            
        }
        public Inventory(CommandStore store)
        {
            _store = store;
        }

        public List<InventoryItem> Items
        {
            get => _itemsByKey.Values.ToList();
            set { _itemsByKey = value.ToDictionary(i => i.Id, i => i); }
        }

        public void AddItem(InventoryItem item)
        {
            _itemsByKey.Add(item.Id,item);
            _store.Add(new CreateInventoryItemCommand(item));
        }

        public void ProcessCommand(InventoryItemCommand command)
        {
            switch (command.Type)
            {
                case InventoryItemCommand.InventoryCommandType.Create:
                    if (command is CreateInventoryItemCommand ciic)
                    {
                        _itemsByKey.Add(ciic.Item.Id,ciic.Item);
                    }
                    break;
                case InventoryItemCommand.InventoryCommandType.Update:
                    break;
                case InventoryItemCommand.InventoryCommandType.Delete:
                    break;
            }
        }
    }
}