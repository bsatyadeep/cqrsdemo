﻿using System.IO;
using System.Xml.Serialization;

namespace InventoryManagement
{
    public class InventoryHelper
    {
        private static string fileName = "inventory.txt";

        public static Inventory Load(CommandStore store)
        {
            if (!File.Exists(fileName))
            {
                return new Inventory(store);
            }

            var xs = new XmlSerializer(typeof(Inventory));
            using (var fs = new FileStream(fileName,FileMode.Open,FileAccess.Read))
            {
                return (Inventory)xs.Deserialize(fs);
            }
        }
        public static Inventory LoadFromStore(CommandStore store)
        {
            var inventory = new Inventory(store);
            foreach (var e in store)
            {
                inventory.ProcessCommand(e);
            }
            return inventory;
        }
        public static void Save(Inventory inventory)
        {
            var xs = new XmlSerializer(typeof(Inventory));
            using (var fs = new FileStream(fileName, FileMode.Create, FileAccess.Write))
            {
                xs.Serialize(fs,inventory);
            }
        }
    }
}