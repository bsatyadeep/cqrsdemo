﻿using System.Xml.Serialization;

namespace InventoryManagement
{
    [XmlInclude(typeof(CreateInventoryItemCommand))]
    public class InventoryItemCommand
    {
        public enum InventoryCommandType
        {
            Create,
            Update,
            Delete
        }

        public InventoryCommandType Type;
    }

    public class CreateInventoryItemCommand:InventoryItemCommand
    {
        public InventoryItem Item;

        public CreateInventoryItemCommand()
        {
            Type = InventoryCommandType.Create;
        }

        public CreateInventoryItemCommand(InventoryItem item):this()
        {
            Item = item;
        }
    }
}