﻿using System.IO;
using System.Xml.Serialization;

namespace InventoryManagement
{
    public class CommandStoreHelper
    {
        private static string fileName = "store.txt";

        public static CommandStore Load()
        {
            if (!File.Exists(fileName))
            {
                return new CommandStore();
            }

            var xs = new XmlSerializer(typeof(CommandStore));
            using (var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                return (CommandStore)xs.Deserialize(fs);
            }
        }

        public static void Save(CommandStore store)
        {
            var xs = new XmlSerializer(typeof(CommandStore));
            using (var fs = new FileStream(fileName, FileMode.Create, FileAccess.Write))
            {
                xs.Serialize(fs, store);
            }
        }
    }
}