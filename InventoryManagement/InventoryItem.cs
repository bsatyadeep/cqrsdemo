﻿namespace InventoryManagement
{
    public class InventoryItem
    {
        public static uint Seed = 0;
        public uint Id;
        public string Name;
        public string Catagory;

        public InventoryItem()
        {
            
        }

        public InventoryItem(string name, string catagory)
        {
            Id = ++Seed;
            Name = name;
            Catagory = catagory;
        }
    }
}