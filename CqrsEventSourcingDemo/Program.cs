﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CqrsEventSourcingDemo
{
    //CQRS: Command Query Responsibility Segregation
    //CQS:  Command Query Separation
    //Command:  DO/Changes

    public class Person
    {
        private int _age;
        private readonly EventBroker _eventBroker;

        public Person(EventBroker eventBroker)
        {
            _eventBroker = eventBroker;
            _eventBroker.Commands += _eventBroker_Commands;
            _eventBroker.Queries += _eventBroker_Queries;
        }

        private void _eventBroker_Queries(object sender, Query query)
        {
            if (query is AgeQuery ac && ac.Target == this)
            {
                ac.Result = _age;
            }
        }

        private void _eventBroker_Commands(object sender, Command command)
        {
            if (command is ChangeAgeCommand cac && cac.Target == this)
            {
                var changedEvent = new AgeChangedEvent(this, _age, cac.Age);
                if (cac.Register)
                    _eventBroker.AllEvents.Add(changedEvent);
                _age = cac.Age;
            }
        }
    }

    public class EventBroker
    {
        public EventBroker()
        {
            AllEvents = new List<Event>();
        }
        //1. Set of All events that happened
        public IList<Event> AllEvents;
        //2. Commands
        public event EventHandler<Command> Commands;

        public void Command(Command c)
        {
            Commands?.Invoke(this,c);
        }
        //3. Query
        public event EventHandler<Query> Queries;
        public T Query<T>(Query query)
        {
            Queries?.Invoke(this,query);
            return (T)query.Result;
        }
        //4. Undo Last Changes
        public void UndoLastChange()
        {
            var e = AllEvents?.LastOrDefault();
            if (e is AgeChangedEvent ac)
            {
                Command(new ChangeAgeCommand(ac.Target,ac.OldValue){Register = false});
                AllEvents.Remove(e);
            }
        }
    }

    public class Query
    {
        public object Result;
    }

    public class AgeQuery : Query
    {
        public Person Target;
    }
    public class Command:EventArgs
    {
        public bool Register = true;
    }

    public class ChangeAgeCommand : Command
    {
        public Person Target;
        public int Age;

        public ChangeAgeCommand(Person target, int age)
        {
            Target = target;
            Age = age;
        }
    }

    public class Event
    {
        //1. Backtrack list of events
    }

    public class AgeChangedEvent : Event
    {
        public Person Target;
        public int OldValue, NewValue;

        public AgeChangedEvent(Person target, int oldValue, int newValue)
        {
            Target = target;
            OldValue = oldValue;
            NewValue = newValue;
        }

        public override string ToString()
        {
            return $"Age changed from {OldValue} to {NewValue}";
        }
    }

    class Program
    {
        static void Main()
        {
            var eb = new EventBroker();
            var people = new Person(eb);

            //Change Object
            eb.Command(new ChangeAgeCommand(people,123));
            //eb.Command(new ChangeAgeCommand(people,39));
            foreach (var e in eb.AllEvents)
            {
                Console.WriteLine(e);
            }
            var age = eb.Query<int>(new AgeQuery{Target = people});
            Console.WriteLine(age);

            //Undo Last Change
            eb.UndoLastChange();
            foreach (var e in eb.AllEvents)
            {
                Console.WriteLine(e);
            }

            age = eb.Query<int>(new AgeQuery { Target = people });
            Console.WriteLine(age);

            Console.ReadKey();
        }
    }
}
